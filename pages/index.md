# Introduction to Types and Terms with algograph - Calculus Of Constructions, to be exact

idea: add googly eyes when introducing a new syntax component

## historical background

Hello, I am the creator of algograph, and you can assume that I have written the rest of this language guide as well.

## Content of this guide

Words are hard. In order to illustrate the rules of algograph better, this guide consists mostly of example algograph code snippets transcribed from other programming languages, or transcribed from algograph to other programming languages.

{{> ../frag/00.md}}